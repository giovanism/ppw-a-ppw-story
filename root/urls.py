from django.urls import re_path
from root.views import index

urlpatterns = [
        re_path(r'^$', index),
    ]
